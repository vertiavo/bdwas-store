package pl.edu.pb.djp.store.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Objects;

@Entity
@IdClass(BookauthorPK.class)
public class Bookauthor {
    private Integer isbn;
    private Integer authorid;
    private Author authorByAuthorid;

    @Id
    @Column(name = "ISBN")
    public Integer getIsbn() {
        return isbn;
    }

    public void setIsbn(Integer isbn) {
        this.isbn = isbn;
    }

    @Id
    @Column(name = "AUTHORID")
    public Integer getAuthorid() {
        return authorid;
    }

    public void setAuthorid(Integer authorid) {
        this.authorid = authorid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bookauthor that = (Bookauthor) o;
        return Objects.equals(isbn, that.isbn) &&
                Objects.equals(authorid, that.authorid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(isbn, authorid);
    }

    @ManyToOne
    @JoinColumn(referencedColumnName = "AUTHORID", nullable = false)
    public Author getAuthorByAuthorid() {
        return authorByAuthorid;
    }

    public void setAuthorByAuthorid(Author authorByAuthorid) {
        this.authorByAuthorid = authorByAuthorid;
    }
}
