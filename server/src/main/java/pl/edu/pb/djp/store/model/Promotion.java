package pl.edu.pb.djp.store.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class Promotion implements Serializable {
    private static final long serialVersionUID = 7003092194205801340L;
    private Integer promotionId;
    private String bookTitle;
    private Integer oldPrice;
    private Integer newPrice;

    @Id
    @Column(name = "PROMOTION_ID")
    public Integer getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(Integer promotionId) {
        this.promotionId = promotionId;
    }

    @Basic
    @Column(name = "BOOK_TITLE")
    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    @Basic
    @Column(name = "OLD_PRICE")
    public Integer getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(Integer oldPrice) {
        this.oldPrice = oldPrice;
    }

    @Basic
    @Column(name = "NEW_PRICE")
    public Integer getNewPrice() {
        return newPrice;
    }

    public void setNewPrice(Integer newPrice) {
        this.newPrice = newPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Promotion promotion = (Promotion) o;
        return Objects.equals(promotionId, promotion.promotionId) &&
                Objects.equals(bookTitle, promotion.bookTitle) &&
                Objects.equals(oldPrice, promotion.oldPrice) &&
                Objects.equals(newPrice, promotion.newPrice);
    }

    @Override
    public int hashCode() {
        return Objects.hash(promotionId, bookTitle, oldPrice, newPrice);
    }
}
