package pl.edu.pb.djp.store.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Objects;

@Entity
@IdClass(OrderitemsPK.class)
public class Orderitems implements Serializable {
    private static final long serialVersionUID = 5916681893416569093L;
    private Integer isbn;
    private Integer orderid;
    private String item;
    private Integer quantity;
    private Books booksByIsbn;

    @Id
    @Column(name = "ISBN")
    public Integer getIsbn() {
        return isbn;
    }

    public void setIsbn(Integer isbn) {
        this.isbn = isbn;
    }

    @Id
    @Column(name = "ORDERID")
    public Integer getOrderid() {
        return orderid;
    }

    public void setOrderid(Integer orderid) {
        this.orderid = orderid;
    }

    @Basic
    @Column(name = "ITEM")
    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    @Basic
    @Column(name = "QUANTITY")
    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Orderitems that = (Orderitems) o;
        return Objects.equals(isbn, that.isbn) &&
                Objects.equals(orderid, that.orderid) &&
                Objects.equals(item, that.item) &&
                Objects.equals(quantity, that.quantity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(isbn, orderid, item, quantity);
    }

    @ManyToOne
    @JoinColumn(referencedColumnName = "ISBN", nullable = false)
    public Books getBooksByIsbn() {
        return booksByIsbn;
    }

    public void setBooksByIsbn(Books booksByIsbn) {
        this.booksByIsbn = booksByIsbn;
    }
}
