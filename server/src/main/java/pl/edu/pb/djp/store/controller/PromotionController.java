package pl.edu.pb.djp.store.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.pb.djp.store.model.Promotion;
import pl.edu.pb.djp.store.repository.PromotionRepository;

import java.util.List;

@RestController
@RequestMapping("/promotions")
public class PromotionController {
    private final PromotionRepository promotionRepository;

    public PromotionController(PromotionRepository promotionRepository) {
        this.promotionRepository = promotionRepository;
    }

    @PostMapping
    public void add(Promotion delivery) {
        this.promotionRepository.save(delivery);
    }

    @GetMapping("/{id}")
    public Promotion get(@PathVariable Integer id) {
        return this.promotionRepository.getOne(id);
    }

    @GetMapping
    public List<Promotion> get() {
        return this.promotionRepository.findAll();
    }

    @DeleteMapping
    public void delete(Integer id) {
        this.promotionRepository.deleteById(id);
    }
}
