package pl.edu.pb.djp.store.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.edu.pb.djp.store.model.Promotion;

@Repository
public interface PromotionRepository extends JpaRepository<Promotion, Integer> {
}
