package pl.edu.pb.djp.store.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.pb.djp.store.model.Publisher;
import pl.edu.pb.djp.store.repository.PublisherRepository;

import java.util.List;

@RestController
@RequestMapping("/publishers")
public class PublisherController {
    private final PublisherRepository publisherRepository;

    public PublisherController(PublisherRepository publisherRepository) {
        this.publisherRepository = publisherRepository;
    }

    @PostMapping
    public void add(@RequestBody Publisher publisher) {
        this.publisherRepository.save(publisher);
    }

    @GetMapping("/{id}")
    public Publisher get(@PathVariable Integer id) {
        return this.publisherRepository.getOne(id);
    }

    @GetMapping
    public List<Publisher> get() {
        return this.publisherRepository.findAll();
    }

    @DeleteMapping
    public void delete(Integer id) {
        this.publisherRepository.deleteById(id);
    }
}
