package pl.edu.pb.djp.store.model;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class OrderitemsPK implements Serializable {
    private Integer isbn;
    private Integer orderid;

    @Column(name = "ISBN")
    @Id
    public Integer getIsbn() {
        return isbn;
    }

    public void setIsbn(Integer isbn) {
        this.isbn = isbn;
    }

    @Column(name = "ORDERID")
    @Id
    public Integer getOrderid() {
        return orderid;
    }

    public void setOrderid(Integer orderid) {
        this.orderid = orderid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderitemsPK that = (OrderitemsPK) o;
        return Objects.equals(isbn, that.isbn) &&
                Objects.equals(orderid, that.orderid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(isbn, orderid);
    }
}
