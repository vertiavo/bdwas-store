package pl.edu.pb.djp.store.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Customers {
    private Integer customerid;
    private String lastname;
    private String firstname;
    private String address;
    private String city;
    private String zip;
    private String referred;

    @Id
    @Column(name = "CUSTOMERID")
    public Integer getCustomerid() {
        return customerid;
    }

    public void setCustomerid(Integer customerid) {
        this.customerid = customerid;
    }

    @Basic
    @Column(name = "LASTNAME")
    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Basic
    @Column(name = "FIRSTNAME")
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @Basic
    @Column(name = "ADDRESS")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "CITY")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Basic
    @Column(name = "ZIP")
    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    @Basic
    @Column(name = "REFERRED")
    public String getReferred() {
        return referred;
    }

    public void setReferred(String referred) {
        this.referred = referred;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customers customers = (Customers) o;
        return Objects.equals(customerid, customers.customerid) &&
                Objects.equals(lastname, customers.lastname) &&
                Objects.equals(firstname, customers.firstname) &&
                Objects.equals(address, customers.address) &&
                Objects.equals(city, customers.city) &&
                Objects.equals(zip, customers.zip) &&
                Objects.equals(referred, customers.referred);
    }

    @Override
    public int hashCode() {
        return Objects.hash(customerid, lastname, firstname, address, city, zip, referred);
    }
}
