package pl.edu.pb.djp.store.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.pb.djp.store.model.Author;
import pl.edu.pb.djp.store.repository.AuthorRepository;

import java.util.List;

@RestController
@RequestMapping("/authors")
public class AuthorController {
    private final AuthorRepository authorRepository;

    public AuthorController(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @PostMapping
    public void add(Author author) {
        this.authorRepository.save(author);
    }

    @GetMapping("/{id}")
    public Author get(@PathVariable Integer id) {
        return this.authorRepository.getOne(id);
    }

    @GetMapping
    public List<Author> get() {
        return this.authorRepository.findAll();
    }

    @DeleteMapping
    public void delete(Integer id) {
        this.authorRepository.deleteById(id);
    }
}
