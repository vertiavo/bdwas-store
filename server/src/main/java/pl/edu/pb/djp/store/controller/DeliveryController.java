package pl.edu.pb.djp.store.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.pb.djp.store.model.Delivery;
import pl.edu.pb.djp.store.repository.DeliveryRepository;

import java.util.List;

@RestController
@RequestMapping("/deliveries")
public class DeliveryController {
    private final DeliveryRepository deliveryRepository;

    public DeliveryController(DeliveryRepository deliveryRepository) {
        this.deliveryRepository = deliveryRepository;
    }

    @PostMapping
    public void add(Delivery delivery) {
        this.deliveryRepository.save(delivery);
    }

    @GetMapping("/{id}")
    public Delivery get(@PathVariable Integer id) {
        return this.deliveryRepository.getOne(id);
    }

    @GetMapping
    public List<Delivery> get() {
        return this.deliveryRepository.findAll();
    }

    @DeleteMapping
    public void delete(Integer id) {
        this.deliveryRepository.deleteById(id);
    }
}
