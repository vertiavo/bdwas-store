package pl.edu.pb.djp.store.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.edu.pb.djp.store.model.Orders;

@Repository
public interface OrdersRepository extends JpaRepository<Orders, Integer> {
}
