package pl.edu.pb.djp.store.model;

import org.hibernate.annotations.Columns;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import pl.edu.pb.djp.store.model.type.AddressType;
import pl.edu.pb.djp.store.model.type.CourierType;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.sql.Time;
import java.util.Objects;

@SuppressWarnings("squid:S1710")
@Entity
@TypeDefs({
        @TypeDef(name = "AddressType", typeClass = AddressType.class),
        @TypeDef(name = "CourierType", typeClass = CourierType.class)
})
public class Delivery implements Serializable {
    private static final long serialVersionUID = -6300895649254815238L;
    private Integer deliveryId;
    private String orderAddres;
    private String orderCourier;
    private Time orderdate;
    private Time shipdate;

    @Id
    @Column(name = "DELIVERY_ID")
    public Integer getDeliveryId() {
        return deliveryId;
    }

    public void setDeliveryId(Integer deliveryId) {
        this.deliveryId = deliveryId;
    }

    @Type(type = "AddressType")
    @Columns(columns = {
            @Column(name = "street"),
            @Column(name = "city"),
            @Column(name = "postal_code")
    })
    public String getOrderAddres() {
        return orderAddres;
    }

    public void setOrderAddres(String orderAddres) {
        this.orderAddres = orderAddres;
    }

    @Type(type = "CourierType")
    @Columns(columns = {
            @Column(name = "full_name"),
            @Column(name = "car"),
            @Column(name = "company_name")
    })
    public String getOrderCourier() {
        return orderCourier;
    }

    public void setOrderCourier(String orderCourier) {
        this.orderCourier = orderCourier;
    }

    @Basic
    @Column(name = "ORDERDATE")
    public Time getOrderdate() {
        return orderdate;
    }

    public void setOrderdate(Time orderdate) {
        this.orderdate = orderdate;
    }

    @Basic
    @Column(name = "SHIPDATE")
    public Time getShipdate() {
        return shipdate;
    }

    public void setShipdate(Time shipdate) {
        this.shipdate = shipdate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Delivery delivery = (Delivery) o;
        return Objects.equals(deliveryId, delivery.deliveryId) &&
                Objects.equals(orderAddres, delivery.orderAddres) &&
//                Objects.equals(orderCourier, delivery.orderCourier) &&
                Objects.equals(orderdate, delivery.orderdate) &&
                Objects.equals(shipdate, delivery.shipdate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deliveryId, orderAddres, orderdate, shipdate);
    }
}
