package pl.edu.pb.djp.store.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.sql.Time;
import java.util.Objects;

@Entity
public class Orders implements Serializable {
    private static final long serialVersionUID = -5622164822604644224L;
    private Integer orderid;
    private Time orderdate;
    private Customers customersByCustomerid;
    private Delivery deliveryByDeliveryId;

    @Id
    @Column(name = "ORDERID")
    public Integer getOrderid() {
        return orderid;
    }

    public void setOrderid(Integer orderid) {
        this.orderid = orderid;
    }

    @Basic
    @Column(name = "ORDERDATE")
    public Time getOrderdate() {
        return orderdate;
    }

    public void setOrderdate(Time orderdate) {
        this.orderdate = orderdate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Orders orders = (Orders) o;
        return Objects.equals(orderid, orders.orderid) &&
                Objects.equals(orderdate, orders.orderdate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderid, orderdate);
    }

    @ManyToOne
    @JoinColumn(name = "CUSTOMERID", referencedColumnName = "CUSTOMERID", nullable = false)
    public Customers getCustomersByCustomerid() {
        return customersByCustomerid;
    }

    public void setCustomersByCustomerid(Customers customersByCustomerid) {
        this.customersByCustomerid = customersByCustomerid;
    }

    @ManyToOne
    @JoinColumn(name = "DELIVERY_ID", referencedColumnName = "DELIVERY_ID")
    public Delivery getDeliveryByDeliveryId() {
        return deliveryByDeliveryId;
    }

    public void setDeliveryByDeliveryId(Delivery deliveryByDeliveryId) {
        this.deliveryByDeliveryId = deliveryByDeliveryId;
    }
}
