package pl.edu.pb.djp.store.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.edu.pb.djp.store.model.Publisher;

@Repository
public interface PublisherRepository extends JpaRepository<Publisher, Integer> {
}
