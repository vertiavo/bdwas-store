package pl.edu.pb.djp.store.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.pb.djp.store.model.Customers;
import pl.edu.pb.djp.store.repository.CustomersRepository;

import java.util.List;

@RestController
@RequestMapping("/customers")
public class CustomersController {
    private final CustomersRepository customersRepository;

    public CustomersController(CustomersRepository customersRepository) {
        this.customersRepository = customersRepository;
    }

    @PostMapping
    public void add(Customers customers) {
        this.customersRepository.save(customers);
    }

    @GetMapping("/{id}")
    public Customers get(@PathVariable Integer id) {
        return this.customersRepository.getOne(id);
    }

    @GetMapping
    public List<Customers> get() {
        return this.customersRepository.findAll();
    }

    @DeleteMapping
    public void delete(Integer id) {
        this.customersRepository.deleteById(id);
    }
}
