package pl.edu.pb.djp.store.model;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class BookauthorPK implements Serializable {
    private Integer isbn;
    private Integer authorid;

    @Column(name = "ISBN")
    @Id
    public Integer getIsbn() {
        return isbn;
    }

    public void setIsbn(Integer isbn) {
        this.isbn = isbn;
    }

    @Column(name = "AUTHORID")
    @Id
    public Integer getAuthorid() {
        return authorid;
    }

    public void setAuthorid(Integer authorid) {
        this.authorid = authorid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookauthorPK that = (BookauthorPK) o;
        return Objects.equals(isbn, that.isbn) &&
                Objects.equals(authorid, that.authorid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(isbn, authorid);
    }
}
