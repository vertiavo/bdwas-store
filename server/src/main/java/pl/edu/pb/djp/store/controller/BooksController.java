package pl.edu.pb.djp.store.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.pb.djp.store.model.Books;
import pl.edu.pb.djp.store.repository.BooksRepository;

import java.util.List;

@RestController
@RequestMapping("/books")
public class BooksController {
    private final BooksRepository booksRepository;

    public BooksController(BooksRepository booksRepository) {
        this.booksRepository = booksRepository;
    }

    @PostMapping
    public void add(Books book) {
        this.booksRepository.save(book);
    }

    @GetMapping("/{id}")
    public Books get(@PathVariable Integer id) {
        return this.booksRepository.getOne(id);
    }

    @GetMapping
    public List<Books> get() {
        return this.booksRepository.findAll();
    }

    @DeleteMapping
    public void delete(Integer id) {
        this.booksRepository.deleteById(id);
    }
}
