package pl.edu.pb.djp.store.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class Publisher implements Serializable {
    private static final long serialVersionUID = -2100996124384291280L;
    private Integer pubid;
    private String name;
    private String contact;
    private Integer phone;

    @Id
    @Column(name = "PUBID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "G1")
    @SequenceGenerator(name = "G1", sequenceName = "PUBLISHER_SEQ", allocationSize = 1)
    public Integer getPubid() {
        return pubid;
    }

    public void setPubid(Integer pubid) {
        this.pubid = pubid;
    }

    @Basic
    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "CONTACT")
    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    @Basic
    @Column(name = "PHONE")
    public Integer getPhone() {
        return phone;
    }

    public void setPhone(Integer phone) {
        this.phone = phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Publisher publisher = (Publisher) o;
        return Objects.equals(pubid, publisher.pubid) &&
                Objects.equals(name, publisher.name) &&
                Objects.equals(contact, publisher.contact) &&
                Objects.equals(phone, publisher.phone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pubid, name, contact, phone);
    }
}
