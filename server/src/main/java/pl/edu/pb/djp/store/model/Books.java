package pl.edu.pb.djp.store.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.sql.Time;
import java.util.Objects;

@Entity
public class Books {
    private Integer isbn;
    private String title;
    private Time pubdate;
    private Publisher publisherByPublisherId;
    private Integer cost;
    private Integer retail;
    private String category;

    @Id
    @Column(name = "ISBN")
    public Integer getIsbn() {
        return isbn;
    }

    public void setIsbn(Integer isbn) {
        this.isbn = isbn;
    }

    @Basic
    @Column(name = "TITLE")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "PUBDATE")
    public Time getPubdate() {
        return pubdate;
    }

    public void setPubdate(Time pubdate) {
        this.pubdate = pubdate;
    }

    @ManyToOne
    @JoinColumn(name = "PUBID", referencedColumnName = "PUBID", nullable = false)
    public Publisher getPublisherByPublisherId() {
        return publisherByPublisherId;
    }

    public void setPublisherByPublisherId(Publisher publisherByPublisherId) {
        this.publisherByPublisherId = publisherByPublisherId;
    }

    @Basic
    @Column(name = "COST")
    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    @Basic
    @Column(name = "RETAIL")
    public Integer getRetail() {
        return retail;
    }

    public void setRetail(Integer retail) {
        this.retail = retail;
    }

    @Basic
    @Column(name = "CATEGORY")
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Books books = (Books) o;
        return Objects.equals(isbn, books.isbn) &&
                Objects.equals(title, books.title) &&
                Objects.equals(pubdate, books.pubdate) &&
                Objects.equals(cost, books.cost) &&
                Objects.equals(retail, books.retail) &&
                Objects.equals(category, books.category);
    }

    @Override
    public int hashCode() {
        return Objects.hash(isbn, title, pubdate, cost, retail, category);
    }
}
