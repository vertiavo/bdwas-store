package pl.edu.pb.djp.store.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.pb.djp.store.model.Orders;
import pl.edu.pb.djp.store.repository.OrdersRepository;

import java.util.List;

@RestController
@RequestMapping("/orders")
public class OrdersController {
    private final OrdersRepository ordersRepository;

    public OrdersController(OrdersRepository ordersRepository) {
        this.ordersRepository = ordersRepository;
    }

    @PostMapping
    public void add(Orders delivery) {
        this.ordersRepository.save(delivery);
    }

    @GetMapping("/{id}")
    public Orders get(@PathVariable Integer id) {
        return this.ordersRepository.getOne(id);
    }

    @GetMapping
    public List<Orders> get() {
        return this.ordersRepository.findAll();
    }

    @DeleteMapping
    public void delete(Integer id) {
        this.ordersRepository.deleteById(id);
    }
}
