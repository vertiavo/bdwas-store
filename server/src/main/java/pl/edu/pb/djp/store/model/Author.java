package pl.edu.pb.djp.store.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class Author implements Serializable {
    private static final long serialVersionUID = -7946328621985339656L;
    private Integer authorid;
    private String lname;
    private String fname;

    @Id
    @Column(name = "AUTHORID")
    public Integer getAuthorid() {
        return authorid;
    }

    public void setAuthorid(Integer authorid) {
        this.authorid = authorid;
    }

    @Basic
    @Column(name = "LNAME")
    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    @Basic
    @Column(name = "FNAME")
    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Author author = (Author) o;
        return Objects.equals(authorid, author.authorid) &&
                Objects.equals(lname, author.lname) &&
                Objects.equals(fname, author.fname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(authorid, lname, fname);
    }
}
